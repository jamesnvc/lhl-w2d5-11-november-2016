//
//  DrawingView.m
//  CustomDrawingStuff
//
//  Created by James Cash on 11-11-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "DrawingView.h"

@implementation DrawingView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {

    CGContextRef ctx = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(ctx, [UIColor redColor].CGColor);

    CGContextFillRect(ctx, rect);

    CGContextSetStrokeColorWithColor(ctx, [UIColor blueColor].CGColor);

    CGContextMoveToPoint(ctx, 10, 10);
    CGContextAddLineToPoint(ctx, 50, 20);
    CGContextAddLineToPoint(ctx, 100, 15);

    CGContextSetLineWidth(ctx, 10);

    CGContextStrokePath(ctx);

    CGContextSetStrokeColorWithColor(ctx, [UIColor greenColor].CGColor);
    CGContextSetFillColorWithColor(ctx, [UIColor colorWithRed:0.1 green:0.2 blue:1.0 alpha:1.0].CGColor);

    UIBezierPath *path = [[UIBezierPath alloc] init];
    [path moveToPoint:CGPointMake(90, 90)];
    [path addArcWithCenter:CGPointMake(50, 50) radius:25 startAngle:M_PI/4 endAngle:M_PI/2 clockwise:YES];
    [path addLineToPoint:CGPointMake(150, 150)];
    [path addLineToPoint:CGPointMake(90, 90)];
    [path fill];
    [path stroke];
}

@end
