//
//  ViewController.m
//  CustomDrawingStuff
//
//  Created by James Cash on 11-11-16.
//  Copyright © 2016 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "DrawingView.h"
@import WebKit;
@import SafariServices;

@interface ViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *inputField;
@property (weak, nonatomic) IBOutlet UILabel *outputLabel;

@end

@implementation ViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    DrawingView *view = [[DrawingView alloc]
                         initWithFrame:CGRectMake(100, 100, 100, 200)];
    [self.view addSubview:view];

    self.inputField.delegate = self;

    WKWebView *webView = [[WKWebView alloc]
                          initWithFrame:CGRectMake(100, 300, 300, 400)];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://duckduckgo.com"]];
    [webView loadRequest:request];

    [self.view addSubview:webView];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Buttons 'n stuff

- (IBAction)processInput:(id)sender {
    SFSafariViewController *safari = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:@"https://httpstatusdogs.com"]];
    [self presentViewController:safari animated:YES completion:^{

    }];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.inputField resignFirstResponder];
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *currentText = textField.text;
    NSString *newText = [currentText stringByReplacingCharactersInRange:range withString:string];
    self.outputLabel.text = [NSString stringWithFormat:@"You typed: %@", newText];
    return YES;
}

@end
